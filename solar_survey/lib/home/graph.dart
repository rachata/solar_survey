import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:solar_survey/theme/colors/light_colors.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class GraphPage extends StatefulWidget {
  @override
  State<GraphPage> createState() => GraphPageState();
}

class GraphPageState extends State<GraphPage> {
  List<GraphData> dataPhase1 = [
    GraphData(0, 10),
    GraphData(1, 100),
    GraphData(2, 0),
    GraphData(3, 0),
    GraphData(4, 0),
    GraphData(5, 0),
    GraphData(6, 0),
    GraphData(7, 50),
    GraphData(8, 110),
    GraphData(9, 120),
    GraphData(10, 50),
    GraphData(11, 55),
    GraphData(12, 79),
    GraphData(13, 70),
    GraphData(14, 50),
    GraphData(15, 30),
    GraphData(16, 20),
    GraphData(17, 110),
    GraphData(18, 50),
    GraphData(19, 0),
    GraphData(20, 0),
    GraphData(21, 0),
    GraphData(22, 0),
    GraphData(23, 0)
  ];

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar: AppBar(
        backgroundColor: LightColors.kDarkYellow,
        title: Text("กราฟ"),
    ),
    backgroundColor: LightColors.kLightYellow,

    body: SingleChildScrollView(
    child: Column(
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      Container(
        height: 500,
        child: makeGraphColumn(),
      ),

    ]

    )
    )
    );

  }

  makeGraphColumn() {
    return SfCartesianChart(
        plotAreaBackgroundColor: Colors.white70,

        primaryYAxis: NumericAxis(
            labelFormat: '{value}',
            title: AxisTitle(
                text: 'หน่วย',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: '',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        primaryXAxis: CategoryAxis(
            title: AxisTitle(
                text: 'เวลา',
                textStyle: ChartTextStyle(
                    color: Colors.blue,
                    fontFamily: '',
                    fontSize: 8,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold))),
        // Chart title
        title: ChartTitle(
            text: 'กราฟแสดงการใช้ไฟฟ้า',
            textStyle: ChartTextStyle(
                color: Colors.blueGrey,
                fontSize: 12,
                fontStyle: FontStyle.normal,
                fontWeight: FontWeight.bold)),
        // Enable legend
        legend: Legend(isVisible: true),
        // Enable tooltip
        tooltipBehavior: TooltipBehavior(enable: true),
        series: <ChartSeries<GraphData, String>>[
          ColumnSeries<GraphData, String>(
            name: "หน่วยไฟฟ้า",
            dataSource: dataPhase1,
            xValueMapper: (GraphData graph, _) => graph.x.toString(),
            yValueMapper: (GraphData graph, _) => graph.y,
            color: Colors.brown,
            width: 0.7,


          ),
        ]);
  }
}

class GraphData {
  GraphData(this.x, this.y);

  final int x;
  final double y;
}