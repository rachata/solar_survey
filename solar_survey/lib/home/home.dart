
import 'package:flutter/material.dart';
import 'package:solar_survey/home/add_home.dart';
import 'package:solar_survey/home/graph.dart';
import 'package:solar_survey/home_list.dart';
import 'package:solar_survey/theme/colors/light_colors.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key key, this.animationController}) : super(key: key);

  final AnimationController animationController;
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage>
    with TickerProviderStateMixin {
  Animation<double> topBarAnimation;

  List<Widget> listViews = <Widget>[];
  final ScrollController scrollController = ScrollController();

  double topBarOpacity = 0.0;

  @override
  void initState() {

    topBarAnimation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: widget.animationController,
            curve: Interval(0, 0.5, curve: Curves.fastOutSlowIn)));
    addAllListData();
    scrollController.addListener(() {
      if (scrollController.offset >= 24) {
        if (topBarOpacity != 1.0) {
          setState(() {
            topBarOpacity = 1.0;
          });
        }
      } else if (scrollController.offset <= 24 &&
          scrollController.offset >= 0) {
        if (topBarOpacity != scrollController.offset / 24) {
          setState(() {
            topBarOpacity = scrollController.offset / 24;
          });
        }
      } else if (scrollController.offset <= 0) {
        if (topBarOpacity != 0.0) {
          setState(() {
            topBarOpacity = 0.0;
          });
        }
      }
    });
    super.initState();
  }

  void addAllListData() {
    const int count = 9;


    String name1 = "โรงงาน A";
    String his1 = "10";
    String his2 = "20";
    String his3 = "30";

    String date1 = "08:00 - 09:00";
    String date2 = "09:00 - 10:00";
    String date3 = "10:00 - 11:00";


    String name2 = "โรงงาน B";
    String his21 = "40";
    String his22 = "20";
    String his23 = "100";

    String date21 = "08:00 - 09:00";
    String date22 = "09:00 - 10:00";
    String date23 = "10:00 - 11:00";







    listViews.add(
        GestureDetector(

          onTap: (){
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => GraphPage()));
          },
          child:       HomeListView(
            animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
                parent: widget.animationController,
                curve:
                Interval((1 / count) * 5, 1.0, curve: Curves.fastOutSlowIn))),
            animationController: widget.animationController,
            name: name1,
            his1: his1,
            his2: his2,
            his3: his3,
            date1: date1,
            date2: date2,
            date3: date3,
          ),

        )

    );

    listViews.add(
      HomeListView(
        animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
            parent: widget.animationController,
            curve:
            Interval((1 / count) * 5, 1.0, curve: Curves.fastOutSlowIn))),
        animationController: widget.animationController,
        name: name2,
        his1: his21,
        his2: his22,
        his3: his23,
        date1: date21,
        date2: date22,
        date3: date23,
      ),
    );


  }

  Future<bool> getData() async {
    await Future<dynamic>.delayed(const Duration(milliseconds: 50));
    return true;
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: LightColors.background,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: LightColors.kDarkYellow,
          title: Text("Home"),
            actions: <Widget>[
        // action button
        IconButton(
        icon: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => AddHomePage()));
        },
      ),
      ]
        ),
        backgroundColor: LightColors.kLightYellow,
        body: Stack(
          children: <Widget>[
            getMainListViewUI(),
//            getAppBarUI(),
            SizedBox(
              height: MediaQuery.of(context).padding.bottom,
            )
          ],

        ),
      ),
    );
  }

  Widget getMainListViewUI() {
    return FutureBuilder<bool>(
      future: getData(),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
        if (!snapshot.hasData) {
          return const SizedBox();
        } else {
          return ListView.builder(
            controller: scrollController,
            padding: EdgeInsets.only(
//              top: AppBar().preferredSize.height +
//                  MediaQuery.of(context).padding.top +
//                  24,
              bottom: 62 + MediaQuery.of(context).padding.bottom,
            ),
            itemCount: listViews.length,
            scrollDirection: Axis.vertical,
            itemBuilder: (BuildContext context, int index) {
              widget.animationController.forward();
              return listViews[index];
            },
          );
        }
      },
    );
  }

  Widget getAppBarUI() {
    return Column(
      children: <Widget>[
        AnimatedBuilder(
          animation: widget.animationController,
          builder: (BuildContext context, Widget child) {
            return FadeTransition(
              opacity: topBarAnimation,
              child: Transform(
                transform: Matrix4.translationValues(
                    0.0, 30 * (1.0 - topBarAnimation.value), 0.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: LightColors.white.withOpacity(topBarOpacity),
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                    ),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: LightColors.grey
                              .withOpacity(0.4 * topBarOpacity),
                          offset: const Offset(1.1, 1.1),
                          blurRadius: 10.0),
                    ],
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: MediaQuery.of(context).padding.top,
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: 16,
                            right: 16,
                            top: 16 - 8.0 * topBarOpacity,
                            bottom: 12 - 8.0 * topBarOpacity),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  'ระบบรายงานการใช้งาน Solar Cell',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontFamily: LightColors.fontName,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 10 + 6 - 6 * topBarOpacity,
                                    letterSpacing: 1.2,
                                    color: LightColors.darkerText,
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                left: 8,
                                right: 8,
                              ),

                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        )
      ],
    );
  }
}
