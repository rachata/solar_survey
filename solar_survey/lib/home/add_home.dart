import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:solar_survey/theme/colors/light_colors.dart';


class AddHomePage extends StatefulWidget {
  const AddHomePage({Key key, this.animationController}) : super(key: key);

  final AnimationController animationController;
  @override
  AddHomePageState createState() => AddHomePageState();
}

class AddHomePageState extends State<AddHomePage>
    with TickerProviderStateMixin {
  Animation<double> topBarAnimation;

  Completer<GoogleMapController> _controller = Completer();
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(7.190569, 100.602610),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target:  LatLng(7.190569, 100.602610),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  List<Widget> listViews = <Widget>[];
  final ScrollController scrollController = ScrollController();

  double topBarOpacity = 0.0;

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      color: LightColors.background,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: LightColors.kDarkYellow,
            title: Text("สร้างการประเมิน"),

        ),
        backgroundColor: LightColors.kLightYellow,

        body: GoogleMap(


          markers: {
            Marker(
                onTap: () {

                },
                markerId: MarkerId("1"),
                position:  LatLng(7.190569, 100.602610),
                infoWindow: InfoWindow(title: "ชัยมงคล", snippet: "AQI : 59")),
            Marker(
                onTap: () {

                },
                markerId: MarkerId("2"),
                position:  LatLng(7.198600, 100.604655),
                infoWindow: InfoWindow(title: "ชายทะเล", snippet: "AQI : 20"))
          },
          mapType: MapType.hybrid,
          initialCameraPosition: _kGooglePlex,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
          },
        ),

//        body: SafeArea(
//          top: true,
//          child: SingleChildScrollView(
//            child: Padding(
//              padding:
//              EdgeInsets.only(left: 16.0, right: 16.0, top: kToolbarHeight),
//              child: Column(
//                children: <Widget>[
//                  Container(
//
//                      child: SizedBox(
//
//                        child: Column(
//
//                          mainAxisAlignment: MainAxisAlignment.spaceAround,
//                          crossAxisAlignment: CrossAxisAlignment.start,
//                          children: <Widget>[
//                            Container(
//                              margin: EdgeInsets.only(left: 10.0, top: 4.0, bottom: 4.0 , right: 10.0),
//                              padding: EdgeInsets.only(left: 16.0, top: 4.0, bottom: 4.0),
//                              decoration: BoxDecoration(
//                                borderRadius: BorderRadius.all(Radius.circular(5)),
//                                color: Colors.white,
//                              ),
//                              child: TextField(
//
//                                decoration: InputDecoration(
//                                    labelText: "ชื่อ",
//                                    border: InputBorder.none),
//                              ),
//                            ),
//                            Container(
//                              margin: EdgeInsets.only(left: 10.0, top: 4.0, bottom: 4.0 , right: 10.0),
//                              padding: EdgeInsets.only(left: 16.0, top: 4.0, bottom: 4.0),
//                              decoration: BoxDecoration(
//                                borderRadius: BorderRadius.all(Radius.circular(5)),
//                                color: Colors.white,
//                              ),
//                              child: TextField(
//
//                                decoration:
//                                InputDecoration(border: InputBorder.none, labelText: 'ขนาด'),
//                              ),
//                            ),
//
//                            Container(
//                              margin: EdgeInsets.only(left: 10.0, top: 4.0, bottom: 4.0 , right: 10.0),
//                              padding: EdgeInsets.only(left: 16.0, top: 4.0, bottom: 4.0),
//                              decoration: BoxDecoration(
//                                borderRadius: BorderRadius.all(Radius.circular(5)),
//                                color: Colors.white,
//                              ),
//                              child: TextField(
//                                onTap: () {
//
//                                },
//                                decoration:
//                                InputDecoration(   border: InputBorder.none, labelText: 'ตำแหน่ง'),
//                              ),
//                            ),
//
//                            Container(
//                              margin: EdgeInsets.only(left: 10.0, top: 4.0, bottom: 4.0 , right: 10.0),
//                              padding: EdgeInsets.only(left: 16.0, top: 4.0, bottom: 4.0),
//
//                              child: getButton(),
//                            ),
//                            Container(
//                              margin: EdgeInsets.only(left: 10.0, top: 4.0, bottom: 4.0 , right: 10.0),
//                              padding: EdgeInsets.only(left: 16.0, top: 4.0, bottom: 4.0),
//
//                              child: Column(
//                                  children: [
//                                    SizedBox(
//                                        width: 100,  // or use fixed size like 200
//                                        height: 100,
//                                        child: GoogleMap(
//                                          mapType: MapType.normal,
//                                          initialCameraPosition: _kGooglePlex,
//                                        ))
//                                  ]
//                              )
//                            )
//
//                          ],
//                        ),
//                      )
//                  ),
//                ],
//              ),
//            ),
//          ),
//        ),
      ),
    );
  }

  Widget getButton(){
    return Padding(
      padding: const EdgeInsets.only(
          left: 16, right: 16, bottom: 16, top: 8),
      child: Container(
        height: 48,
        decoration: BoxDecoration(
          color: LightColors.kGreen,
          borderRadius: const BorderRadius.all(
              Radius.circular(24.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.grey.withOpacity(0.6),
              blurRadius: 8,
              offset: const Offset(4, 4),
            ),
          ],
        ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            borderRadius: const BorderRadius.all(
                Radius.circular(24.0)),
            highlightColor: Colors.transparent,
            onTap: () {

            },
            child: Center(
              child: Text(
                'บันทึก',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                    color: Colors.white),
              ),
            ),
          ),
        ),
      ),
    );
  }


}
