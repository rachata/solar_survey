import 'package:flutter/material.dart';
import 'package:solar_survey/theme/colors/light_colors.dart';

class HomeListView extends StatefulWidget {
  AnimationController animationController;
  Animation animation;

  final String name;

  final String his1;
  final String his2;
  final String his3;

  final String date1;
  final String date2;
  final String date3;

  HomeListView({Key key ,
                this.animationController,
                this.animation  ,
                this.name,
    this.his1, this.his2,
    this.his3, this.date1,this.date2,this.date3} ) : super(key: key);



  @override
  HomeListViewState createState() => HomeListViewState(this.animationController , this.animation);
}

class HomeListViewState extends State<HomeListView> {
  AnimationController animationController;
  Animation animation;

  HomeListViewState(animationController, animation){
    this.animationController = animationController;
    this.animation = animation;
  }
  @override
  void initState(){

  }


  @override
  void deactivate() {

  }

  @override
  void dispose() {


  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: new Transform(
            transform: new Matrix4.translationValues(
                0.0, 30 * (1.0 - animation.value), 0.0),
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 24, right: 24, top: 16, bottom: 18),
              child: Container(
                decoration: BoxDecoration(
                  color: LightColors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8.0),
                      bottomLeft: Radius.circular(8.0),
                      bottomRight: Radius.circular(8.0),
                      topRight: Radius.circular(68.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                        color: LightColors.grey.withOpacity(0.2),
                        offset: Offset(1.1, 1.1),
                        blurRadius: 10.0),
                  ],
                ),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding:
                      const EdgeInsets.only(top: 16, left: 16, right: 24),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 4, bottom: 0, top: 16),
                            child: Text(
                              '${widget.name}',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: LightColors.fontName,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16,
                                  letterSpacing: -0.1,
                                  color: LightColors.darkText),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
//                                  Padding(
//                                    padding: const EdgeInsets.only(
//                                        left: 4, bottom: 3),
//                                    child: Text(
//                                      '',
//                                      textAlign: TextAlign.center,
//                                      style: TextStyle(
//                                        fontFamily: LightColors.fontName,
//                                        fontWeight: FontWeight.w600,
//                                        fontSize: 32,
//                                        color: LightColors.nearlyDarkBlue,
//                                      ),
//                                    ),
//                                  ),
//                                  Padding(
//                                    padding: const EdgeInsets.only(
//                                        left: 8, bottom: 8),
//                                    child: Text(
//                                      'Watt',
//                                      textAlign: TextAlign.center,
//                                      style: TextStyle(
//                                        fontFamily: LightColors.fontName,
//                                        fontWeight: FontWeight.w500,
//                                        fontSize: 18,
//                                        letterSpacing: -0.2,
//                                        color: LightColors.nearlyDarkBlue,
//                                      ),
//                                    ),
//                                  ),
                                ],
                              )
                              ,
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
//                                      Icon(
//                                        Icons.access_time,
//                                        color: FintnessAppTheme.grey
//                                            .withOpacity(0.5),
//                                        size: 16,
//                                      ),
//                                      Padding(
//                                        padding:
//                                        const EdgeInsets.only(left: 4.0),
//                                        child: Text(
//                                          '',
//                                          textAlign: TextAlign.center,
//                                          style: TextStyle(
//                                            fontFamily:
//                                            LightColors.fontName,
//                                            fontWeight: FontWeight.w500,
//                                            fontSize: 14,
//                                            letterSpacing: 0.0,
//                                            color: LightColors.grey
//                                                .withOpacity(0.5),
//                                          ),
//                                        ),
//                                      ),
                                    ],
                                  ),
//                                  Padding(
//                                    padding: const EdgeInsets.only(
//                                        top: 4, bottom: 14),
//                                    child: Text(
//                                      '',
//                                      textAlign: TextAlign.center,
//                                      style: TextStyle(
//                                        fontFamily: LightColors.fontName,
//                                        fontWeight: FontWeight.w500,
//                                        fontSize: 12,
//                                        letterSpacing: 0.0,
//                                        color: LightColors.nearlyDarkBlue,
//                                      ),
//                                    ),
//                                  ),
                                ],
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 24, right: 24, top: 8, bottom: 8),
                      child: Container(
                        height: 2,
                        decoration: BoxDecoration(
                          color: LightColors.background,
                          borderRadius: BorderRadius.all(Radius.circular(4.0)),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 24, right: 24, top: 8, bottom: 16),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  '${widget.his1}',
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontFamily: LightColors.fontName,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16,
                                    letterSpacing: -0.2,
                                    color: LightColors.brown,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 6),
                                  child: Text(
                                    '${widget.date1}',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontFamily: LightColors.fontName,
                                      fontWeight: FontWeight.w600,
                                      fontSize: 10,
                                      color: LightColors.brown
                                          .withOpacity(0.5),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      '${widget.his2}',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: LightColors.fontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                        letterSpacing: -0.2,
                                        color: LightColors.black,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 6),
                                      child: Text(
                                        '${widget.date2}',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: LightColors.fontName,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 10,
                                          color: LightColors.black
                                              .withOpacity(0.5),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Text(
                                      '${widget.his3}',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        fontFamily: LightColors.fontName,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                        letterSpacing: -0.2,
                                        color: LightColors.grey,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 6),
                                      child: Text(
                                        '${widget.date3}',
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                          fontFamily: LightColors.fontName,
                                          fontWeight: FontWeight.w600,
                                          fontSize: 10,
                                          color: LightColors.grey
                                              .withOpacity(0.5),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

