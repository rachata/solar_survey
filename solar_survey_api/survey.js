var app = require('express')();
var mysql = require('mysql');
var crypto = require('crypto');
var request = require('request')
var dateFormat = require('dateformat');
var bodyParser = require('body-parser');
var multer = require('multer');

var port = process.env.PORT || 8888;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

var con = mysql.createConnection({
    host: "34.87.145.147",
    user: "solar",
    password: "@#Solar2020",
    database: "solar"
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});


con.connect(function (err) {
    if (err) throw err;
});


app.post('/login', function (req, res) {


    var username = req.body.username;
    var password = req.body.password;

    password = crypto.createHash('sha256').update(password).digest('hex');

    var query = "select * from survey_user where username = '" + username + "' and password =  '" + password + "'";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            delete result[0]['password']
            res.json({ "status": 200, message: result })
        } else {
            res.json({ "status": 204, message: "The username or password is wrong." })
        }
    });

});


app.post('/regis', function (req, res) {

    var username = req.body.username;
    var password = req.body.password;

    var query = "select * from survey_user  where  username = '"+username+"'";

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        var key = Object.keys(result);

        if (key.length >= 1) {
            res.json({ "status": 204, message: "Username is already taken." })
        } else {

            password = crypto.createHash('sha256').update(password).digest('hex');
            query = "insert into survey_user (username , password) values( '" + username + "' , '" + password + "')";
            con.query(query, function (err, result, fields) {
                if (err) {
                    res.json({ "status": 500, message: err.stack })
                    return;
                }
                res.json({ "status": 200 })


            });


        }

    });


});



app.post('/survey_company', function (req, res) {

    var name = req.body.name;
    var id_user = req.body.id_user;

    var query = "insert into survey_company (name) values ('"+name+"')";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }

        var id_company = result.insertId;
        query = "insert into survey_access (id_user , id_company) values ("+id_user+" , "+id_company+")";
        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
             }

            res.json({ "status": 200, message: result })
        
        });
    });
});


app.post('/survey_access', function (req, res) {

    var id_company = req.body.id_company;
    var id_user = req.body.id_user;

    var query = "insert into survey_access (id_user , id_company) values ("+id_user+" , "+id_company+")";
        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
             }

            res.json({ "status": 200, message: result })
        
        });
});



app.post('/survey_set_power_company', function (req, res) {

    var id_company = req.body.id_company;
    var type = req.body.type;

    var query = "insert into survey_set_power_company (type_set , id_company) values ('"+type+"' , "+id_company+")";
        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
             }

            res.json({ "status": 200, message: result })
        
        });
});


app.post('/survey_data_power_company', function (req, res) {

    var id_set = req.body.id_set;
    var power = req.body.power;

    var query = "insert into survey_data_power_company (id_set , power) values ('"+id_set+"' , "+power+")";
        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
             }

            res.json({ "status": 200, message: result })
        
        });
});





app.post('/', function (req, res) {

    var name = req.body.name;
    var id_user = req.body.id_user;

    var query = "insert into survey_company (id_user , name) values ("+id_user+" , '"+name+"')";
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })
    });

});



app.listen(port, function () {
    console.log('Starting node.js on port ' + port);
});

